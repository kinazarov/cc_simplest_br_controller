local power_gauge = {}
power_gauge.x, power_gauge.y = term.getSize()
local shutdownPercent = 0.85
local startPercent = 0.2
local scriptTimeout = 4
local reactor = peripheral.wrap('right')
local maxPower = 10000000

local function writeGauge(power, symbol)
    local sign = symbol or '#'
    local count = math.floor(power/maxPower * power_gauge.x)
    print('Reactor power is ' .. tostring(power))
    for i = 1, count do io.write(sign) end
    io.write('\n')
end

local shutdown = false
local txtColor = colors.white
while true do
    local power = reactor.getEnergyStored()
    local fillPercent = power / maxPower
    if fillPercent > shutdownPercent then
        txtColor = colors.red
    else
        txtColor = colors.green
    end

    if fillPercent <= startPercent then
        reactor.setActive(true)
    elseif fillPercent > shutdownPercent then
        reactor.setActive(false)
    end

    term.clear()
    term.setTextColor(txtColor)
    writeGauge(power)
    sleep(scriptTimeout)
end